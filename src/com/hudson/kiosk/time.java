package com.hudson.kiosk;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class time {
	public static int year;
	public static int month;
	public static int day;
	public static String clock;

	public static void run() {
		clock = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		year = Calendar.getInstance().get(Calendar.YEAR);
		month = Calendar.getInstance().get(Calendar.MONTH) + 1;
		day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		userdata.date = (day + "/" + month + "/" + year);
	}

}
