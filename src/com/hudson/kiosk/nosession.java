package com.hudson.kiosk;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class nosession extends JFrame {

	public static nosession frame = new nosession();

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					// Primary frame = new Primary();
					frame.setUndecorated(true); // removes programs window borders
					frame.setVisible(false);
					frame.setLocationRelativeTo(null); // sets program to center of screen
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	public nosession() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1920, 1080);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel logout = new JLabel("");
		logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});
		logout.setIcon(new ImageIcon(nosession.class.getResource("/images/logout.png")));
		logout.setBounds(922, 558, 103, 43);
		contentPane.add(logout);

		JLabel Title = new JLabel("");
		Title.setIcon(new ImageIcon(nosession.class.getResource("/images/error.png")));
		Title.setForeground(Color.WHITE);
		Title.setFont(new Font("SansSerif", Font.PLAIN, 99));
		Title.setBounds(873, 299, 200, 173);
		contentPane.add(Title);

		JLabel subtext = new JLabel(config.noclass);
		subtext.setForeground(Color.WHITE);
		subtext.setFont(new Font("SansSerif", Font.PLAIN, 28));
		subtext.setBounds(718, 485, 511, 43);
		contentPane.add(subtext);

		JLabel SIV_logo = new JLabel("");
		SIV_logo.setIcon(new ImageIcon(Primary.class.getResource("/images/logo_small.png")));
		SIV_logo.setBounds(1733, 897, 150, 152);
		contentPane.add(SIV_logo);

		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon(nosession.class.getResource("/images/footer-gradientnew.png")));
		footer.setBounds(0, 842, 1920, 250);
		contentPane.add(footer);

		JLabel Background = new JLabel("");
		Background.setIcon(new ImageIcon(nosession.class.getResource("/images/new background.png")));
		Background.setBounds(0, 0, 1920, 1076);
		contentPane.add(Background);
	}

	public static void run() {
		if (!frame.isUndecorated()) {
			frame.setUndecorated(true);
		}
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}
}
