package com.hudson.kiosk;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import java.awt.SystemColor;

@SuppressWarnings("serial")
public class session extends JFrame {

	public static JLabel sessiontime = new JLabel();
	public static JLabel sessionname = new JLabel();
	public static JLabel sessiontime2 = new JLabel();
	public static JLabel sessionname2 = new JLabel();
	public static JLabel sessiontime3 = new JLabel();
	public static JLabel sessionname3 = new JLabel();

	public static session frame = new session();

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					// Primary frame = new Primary();
					frame.setUndecorated(true); // removes programs window borders
					frame.setVisible(false);
					frame.setLocationRelativeTo(null); // sets program to center of screen
					frame.setAlwaysOnTop(true);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	public session() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1920, 1080);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel logout = new JLabel("");
		logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				userdata.dispose();
				session.update();
				frame.setVisible(false);
				frame.dispose();
				frame.setVisible(false);
				frame.dispose();
			}
		});
		sessionname.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				time.run();
				Primary.print();
				userdata.dispose();
				session.update();
				frame.setVisible(false);
				frame.dispose();
			}
		});
		sessiontime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				time.run();
				Primary.print();
				userdata.dispose();
				session.update();
				frame.setVisible(false);
				frame.dispose();

			}
		});

		sessiontime.setBounds(473, 422, 501, 119);
		contentPane.add(sessiontime);
		sessiontime.setHorizontalAlignment(SwingConstants.CENTER);
		sessiontime.setForeground(Color.WHITE);
		sessiontime.setFont(new Font("Tahoma", Font.BOLD, 20));

		sessionname.setBounds(972, 423, 500, 118);
		contentPane.add(sessionname);
		sessionname.setFont(new Font("Tahoma", Font.BOLD, 20));
		sessionname.setHorizontalAlignment(SwingConstants.CENTER);
		sessionname.setForeground(new Color(255, 255, 255));

		JSeparator separator = new JSeparator();
		separator.setBackground(SystemColor.desktop);
		separator.setForeground(SystemColor.desktop);
		separator.setBounds(499, 543, 947, 2);
		contentPane.add(separator);

		sessiontime2.setBounds(471, 546, 503, 120);
		contentPane.add(sessiontime2);
		sessiontime2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (sessiontime2.getText() != null) {
					time.run();
					Primary.print2();
					userdata.dispose();
					session.update();
					frame.setVisible(false);
					frame.dispose();
				} else {
					System.out.println("Error!: No class is booked here!");
				}
			}
		});
		sessiontime2.setHorizontalAlignment(SwingConstants.CENTER);
		sessiontime2.setForeground(Color.WHITE);
		sessiontime2.setFont(new Font("Tahoma", Font.BOLD, 20));
		sessionname2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (sessionname2.getText() != null) {
					time.run();
					Primary.print2();
					userdata.dispose();
					session.update();
					frame.setVisible(false);
					frame.dispose();
				} else {
					System.out.println("Error!: No class is booked here!");
				}
			}
		});

		sessionname2.setBounds(971, 545, 501, 122);
		contentPane.add(sessionname2);
		sessionname2.setHorizontalAlignment(SwingConstants.CENTER);
		sessionname2.setForeground(Color.WHITE);
		sessionname2.setFont(new Font("Tahoma", Font.BOLD, 20));

		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(SystemColor.desktop);
		separator_1.setBackground(SystemColor.desktop);
		separator_1.setBounds(499, 664, 947, 2);
		contentPane.add(separator_1);
		sessiontime3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (sessionname3.getText() != null) {
					time.run();
					Primary.print3();
					userdata.dispose();
					session.update();
					frame.setVisible(false);
					frame.dispose();
				} else {
					System.out.println("Error!: No class is booked here!");
				}
			}
		});

		sessiontime3.setHorizontalAlignment(SwingConstants.CENTER);
		sessiontime3.setForeground(Color.WHITE);
		sessiontime3.setFont(new Font("Tahoma", Font.BOLD, 20));
		sessiontime3.setBounds(473, 672, 503, 120);
		contentPane.add(sessiontime3);
		sessionname3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (sessionname3.getText() != null) {
					time.run();
					Primary.print3();
					userdata.dispose();
					session.update();
					frame.setVisible(false);
					frame.dispose();
				} else {
					System.out.println("Error!: No class is booked here!");
				}
			}
		});

		sessionname3.setHorizontalAlignment(SwingConstants.CENTER);
		sessionname3.setForeground(Color.WHITE);
		sessionname3.setFont(new Font("Tahoma", Font.BOLD, 20));
		sessionname3.setBounds(973, 671, 501, 122);
		contentPane.add(sessionname3);

		JLabel classbar = new JLabel("");
		classbar.setIcon(new ImageIcon(session.class.getResource("/images/3sessionbar.png")));
		classbar.setBounds(448, 378, 1050, 454);
		contentPane.add(classbar);
		logout.setIcon(new ImageIcon(session.class.getResource("/images/logout.png")));
		logout.setBounds(928, 933, 103, 43);
		contentPane.add(logout);

		JLabel subtext = new JLabel(config.sessionsubtext);
		subtext.setForeground(Color.WHITE);
		subtext.setFont(new Font("SansSerif", Font.PLAIN, 20));
		subtext.setBounds(665, 843, 628, 84);
		contentPane.add(subtext);

		JLabel title = new JLabel(config.sessiontitle);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setForeground(Color.WHITE);
		title.setFont(new Font("SansSerif", Font.PLAIN, 36));
		title.setBounds(191, 292, 1565, 84);
		contentPane.add(title);

		JLabel SIV_logo = new JLabel("");
		SIV_logo.setIcon(new ImageIcon(Primary.class.getResource("/images/logo_small.png")));
		SIV_logo.setBounds(1733, 897, 150, 152);
		contentPane.add(SIV_logo);

		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon(session.class.getResource("/images/footer-gradientnew.png")));
		footer.setBounds(0, 842, 1920, 250);
		contentPane.add(footer);

		JLabel Background = new JLabel("");
		Background.setIcon(new ImageIcon(session.class.getResource("/images/new background.png")));
		Background.setBounds(0, 0, 1920, 1076);
		contentPane.add(Background);
	}

	public static void update() {
		sessiontime.setText("");
		sessionname.setText("");
		sessiontime2.setText("");
		sessionname2.setText("");
		sessiontime3.setText("");
		sessionname3.setText("");
		sessiontime.setText(response.sessiontime);
		sessionname.setText(response.sessionname);
		sessiontime2.setText(response.sessiontime2);
		sessionname2.setText(response.sessionname2);
		sessiontime3.setText(response.sessiontime3);
		sessionname3.setText(response.sessionname3);

	}

	public static void run() {

		if (!frame.isUndecorated()) {
			frame.setUndecorated(true);
		}
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}
}
