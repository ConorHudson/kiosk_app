package com.hudson.kiosk;

import java.util.Timer;
import java.util.TimerTask;

public class timer {
	public static String window;
	public static int i = 0;

	public static void run() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			public void run() {
				if (nosession.frame.isVisible()) {
					window = "nosession";
					timeout();
				} else if (invalid.frame.isVisible()) {
					window = "invalid";
					timeout();
				}
				System.out.println("Timer one running");
				if (i == 1) {
					timer.cancel();
					i = 0;
				}
			}
		}, 0, 4000);

	}

	public static void timeout() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				
				if (i == 1) {
					switch (window) {

					case "nosession":
						nosession.frame.setVisible(false);
						nosession.frame.dispose();
						window = "";
						i = 0;
						break;

					case "session":
						session.frame.setVisible(false);
						session.frame.dispose();
						window = "";
						i = 0;
						break;

					case "invalid":
						invalid.frame.setVisible(false);
						invalid.frame.dispose();
						window = "";
						i = 0;
						break;

					default:
						i = 0;
						break;

					}
					System.out.println("timer two running");
					timer.cancel();
				}
				i++;

			}
		}, 0, 100);
	}
}
