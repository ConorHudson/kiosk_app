package com.hudson.kiosk;

public class config {
	public static String printername;
	public static String sitename;
	public static String titlename;
	public static String subtext;
	public static String invalidID;
	public static String noclass;
	public static String sessiontitle;
	public static String sessionsubtext;

	public static void run() {
		System.out.println("Loading Configuration");

		// here you set configure for hardware and site

		sitename = "Ponds Forge ISC"; // use full site name
		printername = "EPSON TM-T88VI Receipt";
		// supported printers (tested working)
		// EPSON TM-T88VI Receipt
		// EPSON TM-T88IV Receipt

		// MainScreen
		titlename = "SIV Check-in";
		subtext = "Swipe your card or wristband to quickly check in to your Fitness Classes";

		// card/band not recognised screen
		invalidID = "Not recognised please try again!";

		// no classes booked on account screen
		noclass = "You don't have any upcoming bookings.";

		// class selection screen
		sessiontitle = "Welcome, " + response.firstname[0] + "! Tap your booking to retrieve your ticket.";
		sessionsubtext = "Here are your upcoming fitness class bookings within the next 3 hours.";

	}

}
