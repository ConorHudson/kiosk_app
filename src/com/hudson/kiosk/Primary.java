package com.hudson.kiosk;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Primary extends JFrame {
	
	public static receipt printerService = new receipt();

	public static JTextField User_id = new JTextField();

	private JPanel contentPane;

	public static void main(String[] args) throws IOException {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				config.run();
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Primary frame = new Primary();
					frame.setUndecorated(true); // removes programs window borders
					frame.setVisible(true);
					frame.setLocationRelativeTo(null); // sets program to center of screen
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Primary() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1920, 1080);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(25, 25, 112));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel Title = new JLabel(config.titlename);
		Title.setForeground(Color.WHITE);
		Title.setFont(new Font("SansSerif", Font.PLAIN, 99));
		Title.setBounds(668, 262, 611, 212);
		contentPane.add(Title);

		JLabel Subtext = new JLabel(config.subtext);
		Subtext.setForeground(Color.WHITE);
		Subtext.setFont(new Font("SansSerif", Font.PLAIN, 28));
		Subtext.setBounds(513, 485, 920, 43);
		contentPane.add(Subtext);

		JLabel SIV_logo = new JLabel("");
		SIV_logo.setIcon(new ImageIcon(Primary.class.getResource("/images/logo_small.png")));
		SIV_logo.setBounds(1733, 897, 150, 152);
		contentPane.add(SIV_logo);

		JLabel footer = new JLabel("");
		footer.setIcon(new ImageIcon(Primary.class.getResource("/images/footer-gradientnew.png")));
		footer.setBounds(0, 842, 1920, 250);
		contentPane.add(footer);

		JLabel Background = new JLabel("");
		Background.setIcon(new ImageIcon(Primary.class.getResource("/images/new background.png")));
		Background.setBounds(0, 0, 1920, 1076);
		contentPane.add(Background);
		User_id.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					userdata.dispose();
					System.out.println(User_id.getText());
					userdata.sivnumber= (User_id.getText());	
					
					api.run();
					response.run();
					config.run();
					check.run();
					id_exist.run();
					
					
				}

			}
		});

		User_id.setBounds(853, 996, 0, 0);
		contentPane.add(User_id);
		User_id.setColumns(10);

	}
	
	public static void print() {
		//print some stuff. Change the printer name to your thermal printer name.
		   api.complete();
	       printerService.printString(config.printername, "\n\n " + config.sitename); // \n makes  a margin space 
	       printerService.printString(config.printername, "\n\n Group Fitness Ticket");
	       printerService.printString(config.printername, "\n\n " + response.username);
	       printerService.printString(config.printername, "\n\n ID: " + userdata.sivnumber);
	       printerService.printString(config.printername, "\n\n Class: " + response.sessionname);
	       printerService.printString(config.printername, "\n\n Booked For: " + userdata.date + "-" + response.sessiontime);
	       printerService.printString(config.printername, "\n\n Enjoy your class! \n\n\n\n\n\n");
	      
	      
	       

	        // cut that paper!
	        byte[] cutP = new byte[] { 0x1d, 'V', 1 };

	        printerService.printBytes(config.printername, cutP);
	}
	
	public static void print2() {
		
		//print some stuff. Change the printer name to your thermal printer name.
		   printerService.printString(config.printername, "\n\n " + config.sitename); // \n makes  a margin space 
	       printerService.printString(config.printername, "\n\n Group Fitness Ticket");
	       printerService.printString(config.printername, "\n\n " + response.username);
	       printerService.printString(config.printername, "\n\n ID: " + userdata.sivnumber);
	       printerService.printString(config.printername, "\n\n Class: " + response.sessionname2); //response.sessionname + "variable"; try this
	       printerService.printString(config.printername, "\n\n Booked For: " + userdata.date + "-" + response.sessiontime2);
	       printerService.printString(config.printername, "\n\n Enjoy your class! \n\n\n\n\n\n");
	      
	       

	        // cut that paper!
	        byte[] cutP = new byte[] { 0x1d, 'V', 1 };

	        printerService.printBytes(config.printername, cutP);
		
	}
	
	public static void print3() {
		
		//print some stuff. Change the printer name to your thermal printer name in config class.
			printerService.printString(config.printername, "\n\n " + config.sitename); // \n makes  a margin space 
			printerService.printString(config.printername, "\n\n Group Fitness Ticket");
			printerService.printString(config.printername, "\n\n " + response.username);
			printerService.printString(config.printername, "\n\n ID: " + userdata.sivnumber);
			printerService.printString(config.printername, "\n\n Class: " + response.sessionname3);
			printerService.printString(config.printername, "\n\n Booked For: " + userdata.date + "-" + response.sessiontime3);
			printerService.printString(config.printername, "\n\n Enjoy your class! \n\n\n\n\n\n");
	      
	       

	        // cut that paper!
	        byte[] cutP = new byte[] { 0x1d, 'V', 1 };

	        printerService.printBytes(config.printername, cutP);
		
	}
	
}
