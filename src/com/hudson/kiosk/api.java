package com.hudson.kiosk;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class api {
	public static HttpResponse<String> response = null;
	// public static Scanner scanner = new Scanner(System.in);

	public static void run() {
		checkClasses(Primary.User_id.getText());
	}

	public static void checkClasses(String id) {
		HttpClient httpClient = HttpClient.newHttpClient(); // Create new HTTP Client
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://kiosk.siv.org.uk/bookings/list/" + id))
				.build();

		// HttpResponse<String> response = null;

		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (response != null)
			System.out.println(response.body().toString());

	}
	
	public static void complete() {
		HttpClient httpClient = HttpClient.newHttpClient(); // Create new HTTP Client
		HttpRequest complete = HttpRequest.newBuilder().uri(URI.create("https://kiosk.siv.org.uk/bookings/complete")).build();

		// HttpResponse<String> response = null;

		try {
			response = httpClient.send(complete, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (response != null)
			System.out.println("Reply:");
			System.out.println(response.body().toString());

	}
		

}
